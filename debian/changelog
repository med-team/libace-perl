libace-perl (1.92-12) unstable; urgency=medium

  * gcc-14.patch: new: fix build failure with gcc 14.
    Thanks to Niko Tyni <ntyni@debian.org> (Closes: 1075138)
  * d/control: declare compliance to standards version 4.7.0.
  * d/control: add myself to uploaders.
  * d/s/lintian-overrides: flag a false positive error.

 -- Étienne Mollier <emollier@debian.org>  Mon, 05 Aug 2024 13:06:47 +0200

libace-perl (1.92-11) unstable; urgency=medium

  * Team Upload.
  * d/p/cross.patch,d/rules,d/control: Make build cross buildable
    (Closes: #989367)
  * d/README.Debian: Fix file path
  * d/salsa-ci.yml: Disable blhc and reprotest

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 22:17:54 +0530

libace-perl (1.92-10) unstable; urgency=medium

  * Team upload.
  * Make acelib C Makefile respect hardening CFLAGS and CPPFLAGS.
    Closes: #974196

 -- Sascha Steinbiss <satta@debian.org>  Sat, 20 Feb 2021 17:14:04 +0100

libace-perl (1.92-9) unstable; urgency=medium

  [ Matthias Klose ]
  * Build using libtirpc.
    Closes: #974198

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 21 Nov 2020 19:00:09 +0100

libace-perl (1.92-8) unstable; urgency=medium

  * Silence lintian about missing DEB_BUILD_OPTIONS in override_dh_auto_test
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Fix URLs

 -- Andreas Tille <tille@debian.org>  Thu, 25 Oct 2018 17:42:43 +0200

libace-perl (1.92-7) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest failures:
    - skip smoke tests which needs internet access, as during build
    - add debian/tests/pkg-perl/use-name to tell use.t which module to use()
    - add libcgi-pm-perl and libgd-perl to Recommends, otherwise syntax.t
      fails, as some modules need either CGI.pm or GD.pm
    - add patch defined_hash_array.patch to remove the deprecated
      'defined(%hash)' and 'defined(@arry)' usage which causes warnings, which
      make autopkgtest's syntax.t fail

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Nov 2017 18:16:08 +0100

libace-perl (1.92-6) unstable; urgency=medium

  * Only run rpcgen once, not several times in parallel to not fail in
    parallel builds (Thanks for the patch to Adrian Bunk <bunk@debian.org>)
    Closes: #882313
  * Fix some more spelling issues

 -- Andreas Tille <tille@debian.org>  Tue, 21 Nov 2017 14:35:52 +0100

libace-perl (1.92-5) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Testsuite: autopkgtest-pkg-perl
  * debhelper 10
  * Standards-Version: 4.1.1
  * cme fix dpkg-copyright
  * Secure URI in watch file
  * hardening=+all
  * Fix some more spelling issues

 -- Andreas Tille <tille@debian.org>  Mon, 20 Nov 2017 23:09:43 +0100

libace-perl (1.92-4) unstable; urgency=medium

  * Moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control
  * Add copyright statement for debian/*
  * Fix some spelling issues

 -- Andreas Tille <tille@debian.org>  Mon, 01 Feb 2016 17:15:11 +0100

libace-perl (1.92-3) unstable; urgency=low

  * debian/source/format: 3.0 (quilt)
  * debian/control:
     - added myself to Uploaders
     - cme fix dpkg-control
     - debhelper 9
     - canonical Vcs fields
  * debian/README.Debian: Remark about hard coding of build directory
    in probably unused file.
  * debian/rules:
     - use short dh
     - enable propagation of hardening options
  * debian/upstream: Take over content of reference (DOI was wrongly
    specified at URL so I decided to remove)
  * debian/ace.1: automatically created manpage was broken - use help2man
    to create some valid ace.1
  * debian/copyright: DEP5

 -- Andreas Tille <tille@debian.org>  Tue, 29 Oct 2013 11:08:14 +0100

libace-perl (1.92-2) unstable; urgency=low

  * debian/rules
    - Replaced an echo command by the Perl equivalent in debian/rules
      (Closes: #535390).
    - Use dh clean and binary-arch, and created debian/docs and
      debian/examples.
    - Override dh_auto_install, that created duplicates.
  * debian/control:
    - Added links to the Subversion repository where libace-perl is.
    - Incremented Standards-Version to reflect conformance with
      Policy 3.8.2 (no changes needed).
    - Made the Debian Med packaging team the maintainer.
    - Dropped the build dependency on Perl.
  * Updated debian/copyright to my latest exploration of a possible
    machine-readable format.
  * Added bibliographic information in BibTeX format in debian/reference.

 -- Charles Plessy <plessy@debian.org>  Sun, 02 Aug 2009 23:15:09 +0900

libace-perl (1.92-1) unstable; urgency=low

  * Initial Release (Closes: #468760).

 -- Charles Plessy <plessy@debian.org>  Mon, 10 Nov 2008 14:18:23 +0900
